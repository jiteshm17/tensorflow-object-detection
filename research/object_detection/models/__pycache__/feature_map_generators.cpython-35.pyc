
��\J�  �               @   s�   d  Z  d d l Z d d l Z d d l Z d d l m Z e j j Z d Z	 d d �  Z
 d d �  Z Gd	 d
 �  d
 e j j � Z d d d � Z Gd d �  d e j j � Z d d d d d d d � Z d d d � Z d S)a�  Functions to generate a list of feature maps based on image features.

Provides several feature map generators that can be used to build object
detection feature extractors.

Object detection feature extractors usually are built by stacking two components
- A base feature extractor such as Inception V3 and a feature map generator.
Feature map generators build on the base feature extractors and produce a list
of final feature maps.
�    N)�opsg      @c                s   �  � f d d �  } | S)a  Builds a callable to compute depth (output channels) of conv filters.

  Args:
    depth_multiplier: a multiplier for the nominal depth.
    min_depth: a lower bound on the depth of filters.

  Returns:
    A callable that takes in a nominal depth and returns the depth to use.
  c                s   t  |  �  � } t | � � S)N)�int�max)�depthZ	new_depth)�depth_multiplier�	min_depth� �^/home/mohak/Documents/models-master/research/object_detection/models/feature_map_generators.py�multiply_depth0   s    z$get_depth_fn.<locals>.multiply_depthr   )r   r   r
   r   )r   r   r	   �get_depth_fn&   s    
r   c	       
      C   s�   g  }	 |  rY |	 j  t j j j | | | g d d d | d | d | d | j �  �� nD |	 j  t j j j | | | g d | d | d | d | j �  �� |	 j  | j d | o� | d | d	 � � |	 j  | j d | � � |	 S)
a"  Create Keras layers for depthwise & non-depthwise convolutions.

  Args:
    use_depthwise: Whether to use depthwise separable conv instead of regular
      conv.
    kernel_size: A list of length 2: [kernel_height, kernel_width] of the
      filters. Can be an int if both values are the same.
    padding: One of 'VALID' or 'SAME'.
    stride: A list of length 2: [stride_height, stride_width], specifying the
      convolution stride. Can be an int if both strides are the same.
    layer_name: String. The name of the layer.
    conv_hyperparams: A `hyperparams_builder.KerasLayerHyperparams` object
      containing hyperparameters for convolution ops.
    is_training: Indicates whether the feature generator is in training mode.
    freeze_batchnorm: Bool. Whether to freeze batch norm parameters during
      training or not. When training with a small batch size (e.g. 1), it is
      desirable to freeze batch norm update and use pretrained batch norm
      params.
    depth: Depth of output feature maps.

  Returns:
    A list of conv layers.
  r   �   �padding�strides�name�_depthwise_conv�_conv�training�
_batchnorm)	�append�tf�keras�layers�SeparableConv2D�params�Conv2D�build_batch_norm�build_activation_layer)
�use_depthwise�kernel_sizer   �stride�
layer_name�conv_hyperparams�is_training�freeze_batchnormr   r   r   r   r	   �create_conv_block6   s2    		
r$   c                   s7   e  Z d  Z d Z d �  f d d � Z d d �  Z �  S)�KerasMultiResolutionFeatureMapsaB  Generates multi resolution feature maps from input image features.

  A Keras model that generates multi-scale feature maps for detection as in the
  SSD papers by Liu et al: https://arxiv.org/pdf/1512.02325v2.pdf, See Sec 2.1.

  More specifically, when called on inputs it performs the following two tasks:
  1) If a layer name is provided in the configuration, returns that layer as a
     feature map.
  2) If a layer name is left as an empty string, constructs a new feature map
     based on the spatial shape and depth configuration. Note that the current
     implementation only supports generating new layers using convolution of
     stride 2 resulting in a spatial resolution reduction by a factor of 2.
     By default convolution kernel size is set to 3, and it can be customized
     by caller.

  An example of the configuration for Inception V3:
  {
    'from_layer': ['Mixed_5d', 'Mixed_6e', 'Mixed_7c', '', '', ''],
    'layer_depth': [-1, -1, -1, 512, 256, 128]
  }

  When this feature generator object is called on input image_features:
    Args:
      image_features: A dictionary of handles to activation tensors from the
        base feature extractor.

    Returns:
      feature_maps: an OrderedDict mapping keys (feature map names) to
        tensors where each tensor has shape [batch, height_i, width_i, depth_i].
  Nc	                s�  t  t |  � j d | � | |  _ g  |  _ t | | � }	 d }
 d } d | k r\ | d } d } d | k rx | d } x<t | d � D]*\ } } g  } | d | } d } d	 | k r� | d	 | } | r� | }
 n�| r�d
 j |
 | |	 | d � � } | j t	 j
 j j |	 | d � d d g d d d d d | d | j �  �� | j | j d | od| d | d � � | j | j d | � � d j |
 | | | |	 | � � } d } d } | r�d } | d d � } | j t	 j
 j j | � � | r| j t	 j
 j j | | g d d d | d | d | d | j �  �� | j | j d | oX| d | d � � | j | j d | d � � | j t	 j
 j j |	 | � d d g d d d d d | d | j �  �� | j | j d | o�| d | d � � | j | j d | � � n� | j t	 j
 j j |	 | � | | g d | d | d | d | j �  �� | j | j d | oy| d | d � � | j | j d | � � |  j j | � q� Wd S)a�	  Constructor.

    Args:
      feature_map_layout: Dictionary of specifications for the feature map
        layouts in the following format (Inception V2/V3 respectively):
        {
          'from_layer': ['Mixed_3c', 'Mixed_4c', 'Mixed_5c', '', '', ''],
          'layer_depth': [-1, -1, -1, 512, 256, 128]
        }
        or
        {
          'from_layer': ['Mixed_5d', 'Mixed_6e', 'Mixed_7c', '', '', ''],
          'layer_depth': [-1, -1, -1, 512, 256, 128]
        }
        If 'from_layer' is specified, the specified feature map is directly used
        as a box predictor layer, and the layer_depth is directly infered from
        the feature map (instead of using the provided 'layer_depth' parameter).
        In this case, our convention is to set 'layer_depth' to -1 for clarity.
        Otherwise, if 'from_layer' is an empty string, then the box predictor
        layer will be built from the previous layer using convolution
        operations. Note that the current implementation only supports
        generating new layers using convolutions of stride 2 (resulting in a
        spatial resolution reduction by a factor of 2), and will be extended to
        a more flexible design. Convolution kernel size is set to 3 by default,
        and can be customized by 'conv_kernel_size' parameter (similarily,
        'conv_kernel_size' should be set to -1 if 'from_layer' is specified).
        The created convolution operation will be a normal 2D convolution by
        default, and a depthwise convolution followed by 1x1 convolution if
        'use_depthwise' is set to True.
      depth_multiplier: Depth multiplier for convolutional layers.
      min_depth: Minimum depth for convolutional layers.
      insert_1x1_conv: A boolean indicating whether an additional 1x1
        convolution should be inserted before shrinking the feature map.
      is_training: Indicates whether the feature generator is in training mode.
      conv_hyperparams: A `hyperparams_builder.KerasLayerHyperparams` object
        containing hyperparameters for convolution ops.
      freeze_batchnorm: Bool. Whether to freeze batch norm parameters during
        training or not. When training with a small batch size (e.g. 1), it is
        desirable to freeze batch norm update and use pretrained batch norm
        params.
      name: A string name scope to assign to the model. If 'None', Keras
        will auto-generate one from the class name.
    r   � F�use_explicit_paddingr   �
from_layer�layer_depth�   �conv_kernel_sizez{}_1_Conv2d_{}_1x1_{}�   r   r   �SAMEr   r   r   r   z{}_2_Conv2d_{}_{}x{}_s2_{}�VALIDc             S   s   t  j |  | � S)N)r   �fixed_padding)�featuresr   r   r   r	   r/   �   s    z?KerasMultiResolutionFeatureMaps.__init__.<locals>.fixed_paddingr   r   Z_depthwise_batchnorm�
_depthwiseN)�superr%   �__init__�feature_map_layout�convolutionsr   �	enumerate�formatr   r   r   r   r   r   r   r   �Lambda�DepthwiseConv2D)�selfr4   r   r   �insert_1x1_convr"   r!   r#   r   �depth_fn�base_from_layerr'   r   �indexr(   �netr)   r+   r    r   r   r/   )�	__class__r   r	   r3   �   s�    4		

			
		'					
z(KerasMultiResolutionFeatureMaps.__init__c       	      C   s�   g  } g  } x� t  |  j d � D]� \ } } | rL | | } | j | � nO | d } x! |  j | D] } | | � } qd W|  j | d j } | j | � | j | � q  Wt j d d �  t | | � D� � S)a�  Generate the multi-resolution feature maps.

    Executed when calling the `.__call__` method on input.

    Args:
      image_features: A dictionary of handles to activation tensors from the
        base feature extractor.

    Returns:
      feature_maps: an OrderedDict mapping keys (feature map names) to
        tensors where each tensor has shape [batch, height_i, width_i, depth_i].
    r(   r   c             S   s"   g  |  ] \ } } | | f � q Sr   r   )�.0�x�yr   r   r	   �
<listcomp>D  s   	 z8KerasMultiResolutionFeatureMaps.call.<locals>.<listcomp>�����rE   )r6   r4   r   r5   r   �collections�OrderedDict�zip)	r:   �image_features�feature_maps�feature_map_keysr>   r(   �feature_map�layerr    r   r   r	   �call(  s     

z$KerasMultiResolutionFeatureMaps.call)�__name__�
__module__�__qualname__�__doc__r3   rN   r   r   )r@   r	   r%   l   s   
�r%   Fc             C   s�  t  | | � } g  } g  } d }	 d }
 d |  k r= |  d }
 d } d |  k rY |  d } xIt |  d � D]7\ } } |  d | } d } d |  k r� |  d | } | r� | | } | }	 | j | � n�| d } | j �  j �  d } | } | rLd
 j |	 | | | d � � } t j | | | d � d	 d	 g d d d d	 d | �} d j |	 | | | | | � � } d } d } |
 r�d } t j	 | | � } | rTt j
 | d | | g d d	 d | d | d | d �} t j | | | � d	 d	 g d d d d	 d | �} | r�| | | � k r�| t j | d d g d d d d d | d �7} n3 t j | | | � | | g d | d | d | �} | j | � | j | � qj Wt j d d �  t | | � D� � S)a�  Generates multi resolution feature maps from input image features.

  Generates multi-scale feature maps for detection as in the SSD papers by
  Liu et al: https://arxiv.org/pdf/1512.02325v2.pdf, See Sec 2.1.

  More specifically, it performs the following two tasks:
  1) If a layer name is provided in the configuration, returns that layer as a
     feature map.
  2) If a layer name is left as an empty string, constructs a new feature map
     based on the spatial shape and depth configuration. Note that the current
     implementation only supports generating new layers using convolution of
     stride 2 resulting in a spatial resolution reduction by a factor of 2.
     By default convolution kernel size is set to 3, and it can be customized
     by caller.

  An example of the configuration for Inception V3:
  {
    'from_layer': ['Mixed_5d', 'Mixed_6e', 'Mixed_7c', '', '', ''],
    'layer_depth': [-1, -1, -1, 512, 256, 128]
  }

  Args:
    feature_map_layout: Dictionary of specifications for the feature map
      layouts in the following format (Inception V2/V3 respectively):
      {
        'from_layer': ['Mixed_3c', 'Mixed_4c', 'Mixed_5c', '', '', ''],
        'layer_depth': [-1, -1, -1, 512, 256, 128]
      }
      or
      {
        'from_layer': ['Mixed_5d', 'Mixed_6e', 'Mixed_7c', '', '', ''],
        'layer_depth': [-1, -1, -1, 512, 256, 128]
      }
      If 'from_layer' is specified, the specified feature map is directly used
      as a box predictor layer, and the layer_depth is directly infered from the
      feature map (instead of using the provided 'layer_depth' parameter). In
      this case, our convention is to set 'layer_depth' to -1 for clarity.
      Otherwise, if 'from_layer' is an empty string, then the box predictor
      layer will be built from the previous layer using convolution operations.
      Note that the current implementation only supports generating new layers
      using convolutions of stride 2 (resulting in a spatial resolution
      reduction by a factor of 2), and will be extended to a more flexible
      design. Convolution kernel size is set to 3 by default, and can be
      customized by 'conv_kernel_size' parameter (similarily, 'conv_kernel_size'
      should be set to -1 if 'from_layer' is specified). The created convolution
      operation will be a normal 2D convolution by default, and a depthwise
      convolution followed by 1x1 convolution if 'use_depthwise' is set to True.
    depth_multiplier: Depth multiplier for convolutional layers.
    min_depth: Minimum depth for convolutional layers.
    insert_1x1_conv: A boolean indicating whether an additional 1x1 convolution
      should be inserted before shrinking the feature map.
    image_features: A dictionary of handles to activation tensors from the
      base feature extractor.
    pool_residual: Whether to add an average pooling layer followed by a
      residual connection between subsequent feature maps when the channel
      depth match. For example, with option 'layer_depth': [-1, 512, 256, 256],
      a pooling and residual layer is added between the third and forth feature
      map. This option is better used with Weight Shared Convolution Box
      Predictor when all feature maps have the same channel depth to encourage
      more consistent features across multi-scale feature maps.

  Returns:
    feature_maps: an OrderedDict mapping keys (feature map names) to
      tensors where each tensor has shape [batch, height_i, width_i, depth_i].

  Raises:
    ValueError: if the number entries in 'from_layer' and
      'layer_depth' do not match.
    ValueError: if the generated layer does not have the same resolution
      as specified.
  r&   Fr'   r   r(   r)   r*   r+   r   z{}_1_Conv2d_{}_1x1_{}r,   r   r-   r   �scopez{}_2_Conv2d_{}_{}x{}_s2_{}r.   Nr   r1   �_poolc             S   s"   g  |  ] \ } } | | f � q Sr   r   )rA   rB   rC   r   r   r	   rD   �  s   	 z1multi_resolution_feature_maps.<locals>.<listcomp>rE   )r   r6   r   �	get_shape�as_listr7   �slim�conv2dr   r/   �separable_conv2d�
avg_pool2drF   rG   rH   )r4   r   r   r;   rI   Zpool_residualr<   rK   rJ   r=   r'   r   r>   r(   r)   r+   rL   Z	pre_layerZpre_layer_depthZintermediate_layerr    r   r   r   r   r	   �multi_resolution_feature_mapsG  s�    J



				r[   c            	       sF   e  Z d  Z d Z d d d d d d �  f d d � Z d d �  Z �  S)�KerasFpnTopDownFeatureMapsz�Generates Keras based `top-down` feature maps for Feature Pyramid Networks.

  See https://arxiv.org/abs/1612.03144 for details.
  FNc                s'  t  t |  � j d | � |
 r% |
 n d |  _ g  |  _ g  |  _ g  |  _ g  |  _ g  |  _ | rg d n d } d } d } d d �  } |  j j	 t
 j j j | d d g d	 | d
 | d d | | j d d � �� | r� |  j j	 t
 j j j | d d �� x#t t | d � � D]} g  } g  } g  } g  } | j	 t
 j j j | d d g d
 | d	 d d d | d | j d d � �� | r�| j	 t
 j j j | d d �� |	 r�d d �  } | j	 t
 j j j | d d �� n. d d �  } | j	 t
 j j j | d d �� | rAd d �  } | j	 t
 j j j | d d �� | ri| j	 t
 j j j | d d �� | r�| d d � } | j	 t
 j j j | d d �� d | d } t | | | | | | | | | �	 } | j | � |  j j	 | � |  j j	 | � |  j j	 | � |  j j	 | � qWd S)a�  Constructor.

    Args:
      num_levels: the number of image features.
      depth: depth of output feature maps.
      is_training: Indicates whether the feature generator is in training mode.
      conv_hyperparams: A `hyperparams_builder.KerasLayerHyperparams` object
        containing hyperparameters for convolution ops.
      freeze_batchnorm: Bool. Whether to freeze batch norm parameters during
        training or not. When training with a small batch size (e.g. 1), it is
        desirable to freeze batch norm update and use pretrained batch norm
        params.
      use_depthwise: whether to use depthwise separable conv instead of regular
        conv.
      use_explicit_padding: whether to use explicit padding.
      use_bounded_activations: Whether or not to clip activations to range
        [-ACTIVATION_BOUND, ACTIVATION_BOUND]. Bounded activations better lend
        themselves to quantized inference.
      use_native_resize_op: If True, uses tf.image.resize_nearest_neighbor op
        for the upsampling process instead of reshape and broadcasting
        implementation.
      scope: A scope name to wrap this op under.
      name: A string name scope to assign to the model. If 'None', Keras
        will auto-generate one from the class name.
    r   �top_downr.   r-   r   r*   c             S   s   t  j |  t t � S)N)r   �clip_by_value�ACTIVATION_BOUND)r0   r   r   r	   r^     s    z:KerasFpnTopDownFeatureMaps.__init__.<locals>.clip_by_valuer   r   zprojection_%d�use_biasTr^   c             S   s8   |  j  j �  } t j j |  | d d | d d g � S)Nr   r,   )�shaperV   r   �image�resize_nearest_neighbor)rb   �image_shaper   r   r	   rc   5  s    	zDKerasFpnTopDownFeatureMaps.__init__.<locals>.resize_nearest_neighbor�nearest_neighbor_upsamplingc             S   s   t  j |  d d �S)N�scaler,   )r   re   )rb   r   r   r	   re   <  s    zHKerasFpnTopDownFeatureMaps.__init__.<locals>.nearest_neighbor_upsamplingc             S   sK   t  j |  d � } |  d d  d  � d  | d � d  | d � d  d  � f S)Nr   r   r,   )r   ra   )�inputs�residual_shaper   r   r	   �reshapeC  s    z4KerasFpnTopDownFeatureMaps.__init__.<locals>.reshaperi   c             S   s   t  j |  | � S)N)r   r/   )r0   r   r   r   r	   r/   O  s    z:KerasFpnTopDownFeatureMaps.__init__.<locals>.fixed_paddingr/   zsmoothing_%dN)r2   r\   r3   rS   �
top_layers�residual_blocks�top_down_blocks�reshape_blocks�conv_layersr   r   r   r   r   r   r8   �reversed�ranger$   �extend)r:   �
num_levelsr   r"   r!   r#   r   r'   �use_bounded_activations�use_native_resize_oprS   r   r   r   r   r^   �levelZresidual_netZtop_down_netZreshaped_residualZconv_netrc   re   ri   r/   r    �
conv_block)r@   r   r	   r3   �  sr    %					z#KerasFpnTopDownFeatureMaps.__init__c       
      C   s�  g  } g  } t  j |  j � �p| d d } x |  j D] } | | � } q7 W| j | � | j d | d d � t | � } xt t t | d � � � D]� \ } } | | d }	 | d } x! |  j	 | D] } | |	 � }	 q� Wx! |  j
 | D] } | | � } q� Wx' |  j | D] } | |	 | g � } qW| |	 7} x! |  j | D] } | | � } qJW| j | � | j d | | d � q� WWd QRXt j t t t | | � � � � S)a�  Generate the multi-resolution feature maps.

    Executed when calling the `.__call__` method on input.

    Args:
      image_features: list of tuples of (tensor_name, image_feature_tensor).
        Spatial resolutions of succesive tensors must reduce exactly by a factor
        of 2.

    Returns:
      feature_maps: an OrderedDict mapping keys (feature map names) to
        tensors where each tensor has shape [batch, height_i, width_i, depth_i].
    r   ztop_down_%sr   NrE   rE   rE   )r   �
name_scoperS   rj   r   �lenr6   ro   rp   rk   rl   rm   rn   rF   rG   �listrH   )
r:   rI   �output_feature_maps_list�output_feature_map_keysr]   rM   rr   r>   ru   �residualr   r   r	   rN   _  s2    )

$	zKerasFpnTopDownFeatureMaps.call)rO   rP   rQ   rR   r3   rN   r   r   )r@   r	   r\   �  s   rr\   c          "   C   s�  t  j | d � ��t |  � } g  } g  }	 | r7 d n d }
 d } t j t j t j g d |
 d d ���t j |  d d | d d g d d	 d
 d	 d d | �} | r� t  j | t t � } | j	 | � |	 j	 d |  d d � x�t
 t | d � � D]�} | r]t  j d � �? | j j �  } t  j j | | d d | d d g � } Wd	 QRXn t j | d d �} t j |  | d | d d g d d	 d
 d	 d d | d �} | r�t  j | t t � } | rt  j | � } | d	 d	 � d	 | d � d	 | d � d	 d	 � f } | | 7} | r<t  j | t t � } | r]t j t j d d �} n	 t j } | r~t j | | � } | j	 | | | | | g d d | d �� |	 j	 d |  | d � q� Wt j t
 t t |	 | � � � � SWd	 QRXWd	 QRXd	 S)a  Generates `top-down` feature maps for Feature Pyramid Networks.

  See https://arxiv.org/abs/1612.03144 for details.

  Args:
    image_features: list of tuples of (tensor_name, image_feature_tensor).
      Spatial resolutions of succesive tensors must reduce exactly by a factor
      of 2.
    depth: depth of output feature maps.
    use_depthwise: whether to use depthwise separable conv instead of regular
      conv.
    use_explicit_padding: whether to use explicit padding.
    use_bounded_activations: Whether or not to clip activations to range
      [-ACTIVATION_BOUND, ACTIVATION_BOUND]. Bounded activations better lend
      themselves to quantized inference.
    scope: A scope name to wrap this op under.
    use_native_resize_op: If True, uses tf.image.resize_nearest_neighbor op for
      the upsampling process instead of reshape and broadcasting implementation.

  Returns:
    feature_maps: an OrderedDict mapping keys (feature map names) to
      tensors where each tensor has shape [batch, height_i, width_i, depth_i].
  r]   r.   r-   r*   r   r   r   �activation_fnN�normalizer_fnrS   zprojection_%dztop_down_%sr   re   r,   rf   r   zsmoothing_%drE   rE   )r   rw   rx   rW   �	arg_scoperX   rY   r^   r_   r   ro   rp   ra   rV   rb   rc   r   re   �	functools�partialr/   rF   rG   ry   rH   )rI   r   r   r'   rs   rS   rt   rr   rz   r{   r   r   r]   ru   Ztop_down_shaper|   rh   �conv_opr   r   r	   �fpn_top_down_feature_maps�  sf    "		,	6
				r�   c       	      C   s  t  | � d k r t d � � | | j �  d } g  } g  } d |  } |  d k r� t j | |  d d g d d d d d | �} t j | d d g d d d d d | �} | j | � | j | � | } | ret j t j g d d d d	 ��m xe t | d � D]S } d
 j	 | |  � } t j | |  d d g d | �} | j | � | j | � qWWd QRXn� t j t j g d d d d	 ��h x` t | d � D]N } d | } t j | d	 d	 g d d d | �} | j | � | j | � q�WWd QRXt
 j d d �  t | | � D� � S)a�  Generates pooling pyramid feature maps.

  The pooling pyramid feature maps is motivated by
  multi_resolution_feature_maps. The main difference are that it is simpler and
  reduces the number of free parameters.

  More specifically:
   - Instead of using convolutions to shrink the feature map, it uses max
     pooling, therefore totally gets rid of the parameters in convolution.
   - By pooling feature from larger map up to a single cell, it generates
     features in the same feature space.
   - Instead of independently making box predictions from individual maps, it
     shares the same classifier across different feature maps, therefore reduces
     the "mis-calibration" across different scales.

  See go/ppn-detection for more details.

  Args:
    base_feature_map_depth: Depth of the base feature before the max pooling.
    num_layers: Number of layers used to make predictions. They are pooled
      from the base feature.
    image_features: A dictionary of handles to activation tensors from the
      feature extractor.
    replace_pool_with_conv: Whether or not to replace pooling operations with
      convolutions in the PPN. Default is False.

  Returns:
    feature_maps: an OrderedDict mapping keys (feature map names) to
      tensors where each tensor has shape [batch, height_i, width_i, depth_i].
  Raises:
    ValueError: image_features does not contain exactly one entry
  r   z2image_features should be a dictionary of length 1.r   zBase_Conv2d_1x1_%dr   r-   r   rS   r,   zConv2d_{}_3x3_s2_{}r*   NzMaxPool2d_%d_2x2c             S   s"   g  |  ] \ } } | | f � q Sr   r   )rA   rB   rC   r   r   r	   rD   +  s   	 z0pooling_pyramid_feature_maps.<locals>.<listcomp>)rx   �
ValueError�keysrW   rX   �
max_pool2dr   r   rp   r7   rF   rG   rH   )	Zbase_feature_map_depth�
num_layersrI   Zreplace_pool_with_convrK   rJ   Zfeature_map_keyrL   �ir   r   r	   �pooling_pyramid_feature_maps�  sF    "
$"		"
r�   )rR   rF   r�   �
tensorflowr   �object_detection.utilsr   �contribrW   r_   r   r$   r   �Modelr%   r[   r\   r�   r�   r   r   r   r	   �<module>   s"   6���R